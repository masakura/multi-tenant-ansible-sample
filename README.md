# multi tenant deployment
## Setup
```shell
$ . ./bin/activate
$ pip install -r requirements.txt
$ ansible-galaxy install -r requirements.yml
```

## Deployment
```shell
ansible -i inventory/tenant1.yml deployment.yml
```

| tenant name | inventory file
| --- | ---
| Tenant 1 | [inventory/tenant1.yml](inventory/tenant1.yml)
| Tenant 2 | [inventory/tenant2.yml](inventory/tenant2.yml)
| Tenant 3 | [inventory/tenant3.yml](inventory/tenant3.yml)
